'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var compass = require('gulp-compass');
var webserver = require('gulp-webserver');
var gulpFilter = require('gulp-filter');
var mainBowerFiles = require('main-bower-files');
var sftp = require('gulp-sftp');
var html2js = require('gulp-html2js');
var concat = require('gulp-concat');

var paths = {
    js: 'src/*js/**/*.js',
    html: 'src/**/*.html',
    img: 'src/*img/**/*',
    sass: 'src/scss/**/*.scss',
    dist: 'dist'
};
 
gulp.task('html2js', function () {
    return gulp.src('src/template/**/*.html')
        .pipe(html2js({
            base: 'src',
            outputModuleName: 'nm.templates',
            useStrict: true,
            target: 'ng'
        }))
        .pipe(concat('templates.js'))
        .pipe(gulp.dest('dist/js'))
});

gulp.task('bower-publish', function() {
    var jsFilter = gulpFilter('*.js', { restore: true });
    //var cssFilter = gulpFilter('*.css');
    //var fontFilter = gulpFilter(['*.eot', '*.woff', '*.svg', '*.ttf']);

    return gulp.src(mainBowerFiles())

    // grab vendor js files from bower_components, minify and push in /public
    .pipe(jsFilter)
    .pipe(gulp.dest(paths.dist + '/js/'))
    .pipe(jsFilter.restore);

/*
    // grab vendor css files from bower_components, minify and push in /public
    .pipe(cssFilter)
    .pipe(gulp.dest(dest_path + '/css'))
    .pipe(minifycss())
    .pipe(rename({
        suffix: ".min"
    }))
    .pipe(gulp.dest(dest_path + '/css'))
    .pipe(cssFilter.restore())

    // grab vendor font files from bower_components and push in /public
    .pipe(fontFilter)
    .pipe(flatten())*/
    //.pipe(gulp.dest(dest_path + '/fonts'));
});
 
gulp.task('sass', function () {
  gulp.src(paths.sass)
    .pipe(compass({
            config_file: 'config.rb',
            css: 'dist/css',
            sass: 'src/scss'
        }).on('error', sass.logError))
    .pipe(gulp.dest('dist/css'));
});

gulp.task('copy', function() {
    return gulp.src([paths.html, paths.js, paths.img])
        .pipe(gulp.dest('dist'));
});
 
gulp.task('watch', function () {
  gulp.watch([paths.html,
              paths.img, 
              paths.js, 
              paths.sass], ['sass', 'copy']);
});

gulp.task('serve', ['bower-publish', 'sass', 'copy', 'watch'], function() {
  gulp.src('./dist')
    .pipe(webserver({
      livereload: true,
      open: true,
      fallback: 'index.html'
    }));
});