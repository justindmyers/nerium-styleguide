angular.module('nerium.styleguide', ['ui.router', 'mm.foundation', 'nm.templates', 'ngFitText', 'ngLettering']);

angular.module('nerium.styleguide').config(function($stateProvider, $urlRouterProvider, $locationProvider) {
    
    $locationProvider.html5Mode(true);
    
    // For any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise('/');

    // Now set up the states
    $stateProvider.state('home', {
        url: '/',
        templateUrl: 'partials/home.tpl.html'
    });
    
    $stateProvider.state('colors', {
        url: '/colors',
        templateUrl: 'partials/colors.tpl.html'
    });
   
    $stateProvider.state('typography', {
        url: '/typography',
        templateUrl: 'partials/typography.tpl.html'
    });
    
    $stateProvider.state('elements', {
        url: '/elements',
        templateUrl: 'partials/elements.tpl.html'
    });

    $stateProvider.state('elements.alerts', {
        url: '/alerts',
        views: {
            '@': {
                templateUrl: 'partials/alerts.tpl.html'
            }
        }
    });
    
    $stateProvider.state('elements.uiControls', {
        url: '/ui-controls',
        views: {
            '@': {
                templateUrl: 'partials/ui-controls.tpl.html',
            }
        }
    });
    
    $stateProvider.state('elements.dataPresentation', {
        url: '/data-presentation',
        views: {
            '@': {
                templateUrl: 'partials/data-presentation.tpl.html',
            }
        }
    });
    
    $stateProvider.state('elements.navigation', {
        url: '/navigation',
        views: {
            '@': {
                templateUrl: 'partials/navigation.tpl.html',
            }
        }
    });
    
    $stateProvider.state('foundation', {
        url: '/foundation',
        templateUrl: 'partials/foundation.tpl.html'
    }); 
});