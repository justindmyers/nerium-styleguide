add_import_path "vendor/foundation/scss"

http_path = '/'

css_dir = 'dist/css'
sass_dir = 'src/scss'
scss_dir = 'src/scss'
javascript_dir = 'src/js'
fonts_dir = 'src/fonts'
images_dir = 'src/images'